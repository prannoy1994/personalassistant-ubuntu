#!/bin/bash

if [ -z "$1" ]
then
echo "No language supplied, using en_IN\n"
LANG="en-in"
else
echo "using $1 as language\n"
LANG="$1"
fi

API="http://www.google.com/speech-api/v1/recognize?lang=$LANG"

CMD_LIST_DIRECTORY="list directory"
CMD_WHOAMI="who am i"

JSON=`arecord -f cd -t wav -d 6 -r 16000 | flac - -f --best --sample-rate 16000 -o out.flac;\
wget -O - -o /dev/null --post-file out.flac --header="Content-Type: audio/x-flac; rate=16000" "$API"`

UTTERANCE=`echo $JSON\
|sed -e 's/[{}]/''/g'\
|awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]; exit }'\
|awk -F: 'NR==3 { print $3; exit }'\
|sed -e 's/["]/''/g'`

echo "utterance: $UTTERANCE"
echo ""

OPEN="open"
EJECT="cd drive"
BROWSE="browse"
GOOGLE="google"
#chromium-browser $APPLICATION
if [ "${UTTERANCE/$OPEN}" != "$UTTERANCE" ] ; then
  APPLICATION=${UTTERANCE#"open"}
  if [ "${APPLICATION/$EJECT}" != "$APPLICATION" ] ; then
   echo "Opening cd drive" | festival --tts
   eject
  else
  echo "Opening $APPLICATION" | festival --tts
  chromium-browser $APPLICATION
  fi
elif [ "${UTTERANCE/$BROWSE}" != "$UTTERANCE" ] ; then
  DIRECTORY=${UTTERANCE#"browse"}
  echo "Opening File browser" | festival --tts
  gnome-open ~/$(echo "${DIRECTORY//[[:blank:]]/}" | sed 's/.*/\u&/')
elif [ "${UTTERANCE/$GOOGLE}" != "$UTTERANCE" ] ; then
  SEARCH=${UTTERANCE#"google"}
  chromium-browser "google.com/search?q=${SEARCH//[[:blank:]]/}"
  echo "Googling $SEARCH" | festival --tts
fi


if [ `echo "$UTTERANCE" | grep -ic "^$CMD_LIST_DIRECTORY$"` -gt 0 ]; then
ls .
elif [ `echo "$UTTERANCE" | grep -ic "^$CMD_WHOAMI$"` -gt 0 ]; then
whoami
fi
